package org.example.homework1;

import java.util.Objects;

public class Book {
	private final String isbn;
	private final String title;
	private final String author;
	private final int year;

	public Book(final String isbn, final String title, final String author, final int year) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.year = year;
	}

	public String getIsbn() {
		return isbn;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public int getYear() {
		return year;
	}

	@Override
	public String toString() {
		return "Book{" +
				"isbn='" + isbn + '\'' +
				", title='" + title + '\'' +
				", author='" + author + '\'' +
				", year=" + year +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Book book)) {
			return false;
		}
		return year == book.year && Objects.equals(isbn, book.isbn) && Objects.equals(title, book.title) && Objects.equals(author, book.author);
	}

	@Override
	public int hashCode() {
		return Objects.hash(isbn, title, author, year);
	}
}
