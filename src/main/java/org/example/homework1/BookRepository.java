package org.example.homework1;

import java.util.Objects;

public class BookRepository {
	private final Book[] books = new Book[1000];

	public void add(final Book book) {
		for (int i = 0; i < books.length; i++) {
			if (books[i] == null) {
				books[i] = book;
			}
		}
	}

	public void remove(final Book book) {
		for (int i = 0; i < books.length; i++) {
			if (Objects.equals(books[i], book)) {
				books[i] = null;
			}
		}
	}

	public void remove(final int index) {
		if (books[index] == null) {
			throw new NoBookFoundException("Book with id " + index + " is not present");
		}
		books[index] = null;
	}

	public Book search(final String title) {
		for (final Book book : books) {
			if (book != null && Objects.equals(title, book.getTitle())) {
				return book;
			}
		}
		throw new NoBookFoundException("cannot find book with title \"" + title + "\"");
	}

	public Book search(final int index) {
		if (books[index] == null) {
			throw new NoBookFoundException("Book with id " + index + " is not present");
		}
		return books[index];
	}
}
