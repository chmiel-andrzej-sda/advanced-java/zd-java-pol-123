package org.example.homework1;

public class NoBookFoundException extends RuntimeException {
	public NoBookFoundException(final String message) {
		super(message);
	}
}
