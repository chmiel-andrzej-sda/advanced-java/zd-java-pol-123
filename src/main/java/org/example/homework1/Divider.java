package org.example.homework1;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Divider {
	private static final double EPSILON = 0.000000001;

	public static double divide(final double x, final double y) {
		if (y < EPSILON || y > -EPSILON) {
			throw new CannotDivideBy0Exception("y is 0");
		}
		return x / y;
	}

	public static BigDecimal divide(final BigDecimal x, final BigDecimal y) {
		if (y.equals(new BigDecimal("0.0"))) {
			throw new CannotDivideBy0Exception("y is 0");
		}
		return x.divide(y, RoundingMode.UNNECESSARY);
	}
}
