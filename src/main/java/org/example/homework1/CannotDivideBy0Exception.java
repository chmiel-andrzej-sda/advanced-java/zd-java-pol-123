package org.example.homework1;

public class CannotDivideBy0Exception extends ArithmeticException {
	public CannotDivideBy0Exception(final String s) {
		super(s);
	}
}
