package org.example;

public class CustomCheckedException extends Exception {
	public CustomCheckedException(final String message) {
		super(message);
	}
}
