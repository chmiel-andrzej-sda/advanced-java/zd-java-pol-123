package org.example;

public class CustomUncheckedException extends RuntimeException {
	public CustomUncheckedException(final String message) {
		super(message);
	}
}
