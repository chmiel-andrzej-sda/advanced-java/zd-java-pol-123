package org.example;

import org.example.game.Inventory;
import org.example.game.entity.Animal;
import org.example.game.entity.Entity;
import org.example.game.entity.LivingEntity;
import org.example.game.entity.Money;
import org.example.game.entity.NPC;
import org.example.game.entity.Player;
import org.example.game.item.ArmorItem;
import org.example.game.item.Helmet;
import org.example.game.item.Item;
import org.example.game.item.Material;
import org.example.game.item.Metadata;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

public class ZDJavapol123 {
	public static final Scanner SCANNER = new Scanner(System.in);
	public static final Resource RESOURCE = new Resource();
	private static final Random RANDOM = new Random();
	public static final List<? extends Class<? extends Serializable>> CLASSES = List.of(byte.class, char.class, short.class, int.class, long.class, float.class, double.class);

	public static void main(final String[] args) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
//		final Address address = new Address("Krakow", "PL", "Test");
//		final Person person = new Person("Andret", "Test", null);
//		final Person person1 = new Teacher("", "", null, "");
//		final Person[] x = {person, person1, new Student("", "", null, 1)};
		final Player player = new Player("Andret");
		player.getInventory().add(new Helmet("abc", "", Material.IRON, 1, 1));
		final Item item = new ArmorItem("xyz", "", Material.BRONZE, 1, 1);
		item.addMetadata(String.class, new Metadata<>("MeTaDaTa"));
		player.getInventory().add(item);
		System.out.println(item.getMetadata(String.class).getValue());

		final List<Item> items = player.getInventory().getItems();

		items.stream()
				.filter(Objects::nonNull)
				.map(Item::getMetadata)
				.map(map -> map.get(Boolean.class))
				.filter(Objects::nonNull)
				.map(Metadata::getValue)
				.forEach(System.out::println);

		for (final Item i : items) {
			if (i != null) {
				final Metadata<?> metadata = i.getMetadata().get(Integer.class);
				if (metadata != null) {
					System.out.println(metadata.getValue());
				}
			}
		}

		Stream.of(1, 2, 3, 4, 5, 6)
				.filter(x -> x % 2 == 0)
				.forEach(System.out::println);

		player.getInventory().getItems()
				.stream()
				.map(Item::getName)
				.forEach(System.out::println);

		final int sum1 = player.getInventory().getItems()
				.stream()
				.mapToInt(Item::getValue)
				.sum();
		System.out.println(sum1);

		player.getInventory().getItems()
				.stream()
				.filter(i -> i.getMetadata().isEmpty())
				.map(Item::getName)
				.forEach(System.out::println);

		System.out.println(Optional.of(player)
				.filter(x -> x.getName().equals("Andret"))
				.map(LivingEntity::getInventory)
				.orElse(null));

		System.out.println(Optional.of(player).orElseThrow(() -> new IllegalArgumentException("")));

		player.getInventory().getItems()
				.stream()
				.map(Item::getClass)
				.map(Class::getSimpleName)
				.forEach(System.out::println);

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.map(i -> i.get(0))
				.ifPresent(System.out::println);

		System.out.println(Optional.of(player)
				.map(LivingEntity::getHelmet)
				.map(ArmorItem::getMaterial)
				.orElse(null));

//		System.out.println(Optional.of(player)
//				.map(LivingEntity::getGold)
//				.map(Money::getValue)
//				.filter(x -> x > 0)
//				.orElseThrow(IllegalArgumentException::new));

		//		if (item instanceof final ArmorItem armorItem) {
//			armorItem.setValue(4);
//		}
//		final Item item1 = new Item("", "", 10);
//		final Item item2 = new Item("", "", 1);
//		System.out.println(item1.equals(item2));

		final Person[] people = {
				new Student("B", "A", new Address("X", "Y", "Z"), Gender.MALE, 1),
				new Teacher("A", "A", null, Gender.FEMALE, ""),
				new Person("", "", null, Gender.FEMALE) {
					@Override
					public void work() {
						System.out.println("I'm temporary");
					}
				},
				new Person("test", "", null, Gender.MALE) {
					@Override
					public void work() {
						System.out.println(name);
					}
				},
		};

		for (final Person person : people) {
//			person.work();
		}

		new NPC("Adam").speak("Hello");
		new Player("Adam").speak("Hello");
		new Animal("Adam").speak("Hello");

		final Walkable walkable = new Teacher("", "", null, Gender.MALE, "");
		walkable.walk(10);

		final Walkable w = new Walkable() {
			@Override
			public void walk(final int steps) {
//				System.out.println("anonymous class walked " + steps + " steps");
			}
		};
		w.walk(10);

		final Money<Integer> money = new Money<>(10);
		final Money<Float> money1 = new Money<>(10.0f);
		player.setGold(new Money<>(100));

//		final Person.Pet pet = people[0].new Pet("Loli");
//		System.out.println(pet.name);
//		System.out.println("================== exceptions: ");
//		System.out.println(player.getInventory().getItem(9999999).getName());
		try {
			throwing1();
		} catch (final IllegalArgumentException e) {
//			e.printStackTrace();
		}
		try {
			throwing2();
		} catch (final ReflectiveOperationException e) {
//			e.printStackTrace();
		}
		try {
			throwing3();
		} catch (final CustomUncheckedException e) {
//			e.printStackTrace();
		}
		try {
			throwing4();
		} catch (final CustomCheckedException e) {
//			e.printStackTrace();
		}
		System.out.println("x");
//		final int result = readInt();
//		System.out.println(result);
//		x1(10);
//		System.out.println(fibonacci(47));
//		System.out.println(fibonacci1(70));
		// using recursive calls:
		// 1. Sum of 1..n
		// 2. Factorial
		// 3*. The same using iteration
//		System.out.println(sumRecursive(10_000));
//		System.out.println(sumIterative(10_000));
//		System.out.println(factorialRecursive(6));
//		System.out.println(factorialIterative(6));
//		if (Objects.equals(player.getInventory().getItem(10).getName(), "Test")) {
//
//		}
//		System.out.println(Divider.divide(5, 0.1 + 0.2 - 0.3));

//		System.out.println(Divider.divide(
//				new BigDecimal(5),
//				new BigDecimal("0.1").add(new BigDecimal("0.2").subtract(new BigDecimal("0.3")))));
		final List<Address> addresses = people[0].getAddresses();
		for (final Address address : addresses) {
			System.out.println(address);
		}
		addresses.clear();

		final Set<Person> set = new TreeSet<>();
		set.add(people[0]);
		set.add(people[1]);
		set.add(people[0]);
		set.forEach(System.out::println);
		System.out.println(set.size());

		final Set<Entity> hashSet = new HashSet<>();
		final Set<Entity> treeSet = new TreeSet<>(new Comparator<Entity>() {
			@Override
			public int compare(final Entity o1, final Entity o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		hashSet.add(new Entity("test"));
		treeSet.add(new Entity("test"));

		hashSet.add(new Entity("fox"));
		treeSet.add(new Entity("fox"));

		hashSet.add(new Entity("test"));
		treeSet.add(new Entity("test"));

		hashSet.add(new Entity("fox"));
		treeSet.add(new Entity("fox"));

		treeSet.forEach(System.out::println);

		hashSet.forEach(System.out::println);

		final List<Integer> list = List.of(1, 2, 1, 2, 1, 2, 1, 2);
		final List<Integer> target = new ArrayList<>(new HashSet<>(list));
		System.out.println(target);

		final Map<String, Person> map = new HashMap<>();
		map.put("", people[1]);
		final Set<String> strings = map.keySet();
		final Collection<Person> values = map.values();
		map.put("a", people[1]);
		final Person t = map.getOrDefault("test", new Teacher("", "", null, Gender.MALE, ""));
		final Map<Class<?>, String> someMap = new HashMap<>();
		someMap.put(Person.class, "Person");
		someMap.get(Person.class);

		final Test test = new Test() {
			@Override
			public void a(final String s) {
				System.out.println(s);
			}
		};

		final Test test1 = ZDJavapol123::xyz;
		test.a("s");
		test1.a("a");

		final ToIntFunction<String> toInt = String::length;
		System.out.println(toInt.applyAsInt("abcd"));

		final Function<String, Entity> f = Animal::new;
		System.out.println(f.apply("test"));

		final Consumer<String> x = System.out::println;
		x.accept("abc");

		final Supplier<String> s = ZDJavapol123::create;
		System.out.println(s.get());

		final Predicate<String> p = String::isBlank;
		System.out.println(p.test("   "));

		final Function<Integer, Integer> sum = n -> {
			int result = 0;
			for (int i = 1; i <= n; i++) {
				result += i;
			}
			return result;
		};
		System.out.println(sum.apply(100));

		final IntSupplier r = () -> RANDOM.nextInt(10);
		System.out.println(r.getAsInt());

		final Consumer<String> c = arg -> {
			for (final char chr : arg.toCharArray()) {
				System.out.println(chr);
			}
		};

		c.accept("Ala ma kota");

		final IntPredicate prime = n -> {
			for (int i = 2; i < n; i++) {
				if (n % i == 0) {
					return false;
				}
			}
			return true;
		};

		System.out.println(prime.test(17));
		System.out.println(prime.test(29));
		System.out.println(prime.test(24));
		streaming(true);

		Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
				.filter(integer -> integer % 2 == 0)
				.map(y -> y * y)
				.mapToDouble(y -> y / 10.)
				.filter(y -> y < 0);

		Arrays.stream(people)
				.map(Person::getAddresses)
				.flatMap(Collection::stream)
				.forEach(System.out::println);

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.mapToInt(Item::getValue)
				.average()
				.orElse(0.0);

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.map(Item::getMetadata)
				.map(Map::entrySet)
				.flatMap(Collection::stream)
				.count();

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.filter(inventoryItem -> inventoryItem.getMetadata().size() > 0)
				.toList();

		// =================

		final File file = new File(".");
		final File[] files = file.listFiles();
		System.out.println(Arrays.toString(files));

		final Path path = Paths.get("src", "main", "java", "org", "example", "ZDJavapol123.java");
		final List<String> allLines = Files.readAllLines(path).stream().filter(str -> str.startsWith("import")).toList();
		System.out.println(allLines);

		Files.createDirectories(Paths.get("test"));
		final Path path1 = Paths.get("test", "file.txt");
		final PrintWriter printWriter = new PrintWriter(path1.toFile());
		printWriter.println("Yes");
		printWriter.close();

		final Player p1 = new Player("test");
		final Player p2 = new Player("Andret");
		p1.setGold(new Money<>(20));
		p1.setHealth(7);

		Files.createDirectories(Paths.get("players"));
		final Path p1Path = Paths.get("players", p1.getName() + ".txt");
		final PrintWriter p1Pw = new PrintWriter(p1Path.toFile());
		p1Pw.write(p1.toString());
		p1Pw.close();

		final Path p2Path = Paths.get("players", p2.getName() + ".txt");
		final PrintWriter p2Pw = new PrintWriter(p2Path.toFile());
		p2Pw.write(p2.toString());
		p2Pw.close();

		System.out.println(Arrays.toString(new File("players").listFiles()));
		System.out.println(readPlayer("Andret"));

		// ================
//		final Thread thread1 = new Thread(() -> {
//			for (int i = 1; i <= 20; i++) {
//				System.out.println(i);
//				try {
//					Thread.sleep(500);
//				} catch (final InterruptedException e) {
//					Thread.currentThread().interrupt();
//				}
//			}
//		});
//		thread1.start();

//		final Thread thread2 = new Thread(() -> {
//			for (int i = 0; i < 10; i++) {
//				System.out.println(RANDOM.nextInt(100));
//				System.out.println(Thread.currentThread().getName());
//				try {
//					Thread.sleep(600);
//				} catch (final InterruptedException e) {
//					Thread.currentThread().interrupt();
//				}
//			}
//		});
//		thread2.start();

//		final Thread thread3 = new Thread(() -> {
//			final int n = SCANNER.nextInt();
//			System.out.println(n * n);
//			System.out.println(n * n * n);
//		});
//		thread3.start();
//		final Thread thread1 = new CustomThread(500);
//		thread1.start();
//
//		final Thread thread2 = new CustomThread(550);
//		thread2.start();
//
//		final Thread thread3 = new CustomThread(475);
//		thread3.start();
//
//		final ThreadTest threadTest = new ThreadTest();

//		new Thread(() -> {
//			try {
//				threadTest.setX(1);
//			} catch (final InterruptedException e) {
//				throw new RuntimeException(e);
//			}
//		}).start();
//		new Thread(() -> {
//			try {
//				threadTest.setX(6);
//			} catch (final InterruptedException e) {
//				throw new RuntimeException(e);
//			}
//		}).start();
//		new Thread(() -> {
//			try {
//				System.out.println(threadTest.getX());
//			} catch (final InterruptedException e) {
//				throw new RuntimeException(e);
//			}
//		}).start();

		new Thread(() -> {
			for (int i = 0; i <= 10; i++) {
				RESOURCE.add(i);
			}
		}).start();

		new Thread(() -> {
			for (int i = 0; i <= 10; i++) {
				System.out.println(RESOURCE.getList());
			}
		}).start();

		final Address address = new Address("X", "Y", "Z");
		final Method print = Address.class.getDeclaredMethod("print", int.class);
		print.setAccessible(true);
		print.invoke(address, 3); // address.print(3)
		final ReflectiveClass reflectiveClass = new ReflectiveClass();
		final Method[] methods = ReflectiveClass.class.getDeclaredMethods();
		Arrays.stream(methods)
				.forEach(m -> {
					System.out.println(m.getReturnType());
					m.setAccessible(true);
					try {
						m.invoke(reflectiveClass);
					} catch (final IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}
				});
		final Field street = Address.class.getDeclaredField("street");
		street.setAccessible(true);
		street.set(address, "test");
		System.out.println(address);
		System.out.println(street.get(address));
		List.of(int.class, double.class).contains(street.getType());
		if (street.getType().equals(int.class)) {

		}

		final Person student = new Student("", "", new Address(), Gender.MALE, 1);
		reset(student);
		System.out.println(student);

		Arrays.stream(ZDJavapol123.class.getDeclaredMethods())
				.filter(m -> m.isAnnotationPresent(MyAnnotation.class))
				.forEach(method -> {
					final MyAnnotation annotation = method.getAnnotation(MyAnnotation.class);
					System.out.println(method.getName() + ", " + annotation.value() + ", " + annotation.anotherValue());
				});
		Arrays.stream(ReflectiveClass.class.getDeclaredMethods())
				.sorted((o1, o2) -> {
					final Priority o1Annotation = o1.getAnnotation(Priority.class);
					final Priority o2Annotation = o2.getAnnotation(Priority.class);
					final int o1Value = o1Annotation == null ? 0 : o1Annotation.value();
					final int o2Value = o2Annotation == null ? 0 : o2Annotation.value();
					return o2Value - o1Value;
				})
				.filter(method -> !method.isAnnotationPresent(Ignore.class))
				.forEach(method -> {
					method.setAccessible(true);
					final Priority annotation = method.getAnnotation(Priority.class);
					if (annotation != null) {
						System.out.print(annotation.description() + " " + annotation.value() + ": ");
					}
					try {
						method.invoke(reflectiveClass);
					} catch (final IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}
				});
	}

	public static void reset(final Object o) {
		Stream.concat(Arrays.stream(o.getClass().getSuperclass().getDeclaredFields()),
						Arrays.stream(o.getClass().getDeclaredFields()))
				.forEach(field -> {
					System.out.println(field);
					field.setAccessible(true);
					try {
						if (CLASSES.contains(field.getType())) {
							field.set(o, 0);
						} else if (field.getType().equals(boolean.class)) {
							field.set(o, false);
						} else {
							field.set(o, null);
						}
					} catch (final IllegalAccessException e) {
						e.printStackTrace();
					}
				});
	}

	public static void read() {
		try {
			Thread.sleep(1000);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		System.out.println("text");
	}

	public static String readPlayer(final String playerName) throws IOException {
		return String.join("\n", Files.readAllLines(Paths.get("players", playerName + ".txt")));
	}

	public static Stream<Double> streaming(final boolean flag) {
		if (!flag) {
			return Stream.empty();
		}
		return Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
				.filter(integer -> integer % 2 == 0)
				.map(x -> x * x)
				.map(x -> x / 10.);
	}

	public static String create() {
		return "x";
	}

	public static void xyz(final String s) {
		System.out.println(s);
	}

	public static int sumRecursive(final int n) {
		if (n == 1) {
			return 1;
		}
		return n + sumRecursive(n - 1);
	}

	public static int sumIterative(final int n) {
		int result = 1;
		for (int i = 2; i <= n; i++) {
			result += i;
		}
		return result;
	}

	public static int factorialRecursive(final int n) {
		if (n == 1) {
			return 1;
		}
		return n * factorialRecursive(n - 1);
	}

	public static int factorialIterative(final int n) {
		int result = 1;
		for (int i = 2; i <= n; i++) {
			result *= i;
		}
		return result;
	}

	public static <T extends Number> Box<T> print(final Box<T> box) {
		System.out.println(box.getValue().toString());
		return box;
	}

	public static void throwing1() {
		throw new IllegalArgumentException("oops");
	}

	public static void throwing2() throws ReflectiveOperationException {
		throw new ReflectiveOperationException("oops");
	}

	public static void throwing3() {
		throw new CustomUncheckedException("oops");
	}

	public static void throwing4() throws CustomCheckedException {
		throw new CustomCheckedException("oops");
	}

	public static void x1(final int x) {
		if (x == 0) {
			return;
		}
		System.out.println("before " + (x - 1));
		x1(x - 1);
		System.out.println("after " + (x - 1));
	}

	public static long fibonacci(final int n) {
		if (n < 2) {
			return 1;
		}
		return fibonacci(n - 1) + fibonacci(n - 2);
	}

	public static long fibonacci1(final int n) {
		final long[] result = {0, 1, 1};
		for (int i = 0; i < n; i++) {
			result[0] = result[1];
			result[1] = result[2];
			result[2] = result[0] + result[1];
		}
		return result[1];
	}

	@MyAnnotation(value = "test", anotherValue = 8)
	public static void x2() {

	}

	@MyAnnotation(value = "", anotherValue = 1)
	public static int readInt() {
		final Scanner scanner = new Scanner(System.in);
		while (true) {
			try {
				final int result = scanner.nextInt();
				scanner.nextLine();
				return result;
			} catch (final InputMismatchException ex) {
				scanner.nextLine();
				System.out.println("It's not a number");
			}
		}
	}
}

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface Priority {
	int value() default 0;

	String description() default "";
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Ignore {
}

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@interface MyAnnotation {
	String value();

	int anotherValue() default -1;
}

class ReflectiveClass {
	@Priority(value = 3, description = "public")
	public void publicMethod() {
		System.out.println("I'm public");
	}

	@Priority(value = -3, description = "protected")
	protected void protectedMethod() {
		System.out.println("I'm protected");
	}

	@Priority(description = "package-private")
	void packagePrivateMethod() {
		System.out.println("I'm package-private");
	}

	@Priority(value = 1)
	@Ignore
	private void privateMethod() {
		System.out.println("I'm private");
	}
}

class ThreadTest {
	int x;

	public synchronized int getX() throws InterruptedException {
		Thread.sleep(1000);
		return x;
	}

	public synchronized void setX(final int x) throws InterruptedException {
		Thread.sleep(1000);
		this.x = x;
	}
}

class Resource {
	private final List<Integer> list = new ArrayList<>();

	public synchronized List<Integer> getList() {
		try {
			Thread.sleep(2000);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		return list;
	}

	public synchronized void add(final int x) {
		try {
			Thread.sleep(2000);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		list.add(x);
	}

	public void remove(final int x) {
		try {
			Thread.sleep(2000);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		list.remove(x);
	}
}

class CustomThread extends Thread {
	private final int interval;

	public CustomThread(final int interval) {
		this.interval = interval;
	}

	@Override
	public void run() {
		for (int i = 0; i <= 10; i++) {
			System.out.println(Thread.currentThread().getName() + ": " + i);
			try {
				Thread.sleep(interval);
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}

class Address {
	private String street;
	private String city;
	private String country;

	public Address(final String street, final String city, final String country) {
		this.street = street;
		this.city = city;
		this.country = country;
	}

	public Address(final String street, final String city) {
		this(street, city, "");
	}

	public Address() {
		this("", "");
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(final String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	private void print(final int x) {
		System.out.println(x + ": " + street + ", " + city + ", " + country);
	}

	@Override
	public String toString() {
		return "Address{" +
				"street='" + street + '\'' +
				", city='" + city + '\'' +
				", country='" + country + '\'' +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Address address)) {
			return false;
		}
		return Objects.equals(street, address.street) && Objects.equals(city, address.city) && Objects.equals(country, address.country);
	}

	@Override
	public int hashCode() {
		return Objects.hash(street, city, country);
	}
}

enum Gender {
	MALE("mężczyzna"),
	FEMALE("kobieta");

	private final String name;

	Gender(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}

abstract class Person implements Comparable<Person> {
	protected final String name;
	final String surname;
	private final List<Address> addresses = new ArrayList<>();
	private Gender gender;
	private Pet pet = new Pet("");

	public class Pet {
		String name;

		Pet(final String name) {
			this.name = name;
//			System.out.println(Person.this.name);
		}
	}

	protected Person(final String name, final String surname, final Address address, final Gender gender) {
		this.name = name;
		this.surname = surname;
		addresses.add(address);
		this.gender = gender;
		final String test = new Person.Pet("").name;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public Gender getGender() {
		return gender;
	}

	public abstract void work();

	@Override
	public String toString() {
		return "Person{" +
				"name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", address=" + addresses +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Person person)) {
			return false;
		}
		return Objects.equals(name, person.name) && Objects.equals(surname, person.surname) && Objects.equals(addresses, person.addresses);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, surname, addresses);
	}

	@Override
	public int compareTo(final Person o) {
		if (surname.equals(o.surname)) {
			return name.compareTo(o.name);
		}
		return surname.compareTo(o.surname);
	}
}

class Box<T> {
	private T value;

	public Box(final T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(final T value) {
		this.value = value;
	}
}

class Student extends Person {
	private final int indexNumber;

	public Student(final String name, final String surname, final Address address, final Gender gender, final int indexNumber) {
		super(name, surname, address, gender);
		this.indexNumber = indexNumber;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	@Override
	public void work() {
		System.out.println("I'm studying");
	}

	@Override
	public String toString() {
		return "Student{" +
				"indexNumber=" + indexNumber +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Student student)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return indexNumber == student.indexNumber;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), indexNumber);
	}
}

class Teacher extends Person implements Walkable {
	public final String subject;

	public Teacher(final String name, final String surname, final Address address, final Gender gender, final String subject) {
		super(name, surname, address, gender);
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	@Override
	public void work() {
		System.out.println("I'm teaching");
	}

	@Override
	public String toString() {
		return "Teacher{" +
				"subject='" + subject + '\'' +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Teacher teacher)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return Objects.equals(subject, teacher.subject);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), subject);
	}

	@Override
	public void walk(final int steps) {
		System.out.println("Teacher walked " + steps + " steps!");
	}
}

interface Walkable {
	void walk(int steps);
}

@FunctionalInterface
interface Test {
	void a(String s);
}
