package org.example.family;

import java.util.List;
import java.util.Objects;

public class Human {
	private final String name;
	private final List<Animal> animals;

	public Human(final String name, final List<Animal> animals) {
		this.name = name;
		this.animals = animals;
	}

	public String getName() {
		return name;
	}

	public List<Animal> getAnimals() {
		return animals;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Human human)) {
			return false;
		}
		return Objects.equals(name, human.name) && Objects.equals(animals, human.animals);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, animals);
	}

	@Override
	public String toString() {
		return "Human{" +
				"name='" + name + '\'' +
				", animals=" + animals +
				'}';
	}
}
