package org.example.family;

import java.util.Objects;

public class Animal {
	private final String name;
	private final int years;

	public Animal(final String name, final int years) {
		this.name = name;
		this.years = years;
	}

	public String getName() {
		return name;
	}

	public int getYears() {
		return years;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Animal animal)) {
			return false;
		}
		return years == animal.years && Objects.equals(name, animal.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, years);
	}

	@Override
	public String toString() {
		return "Animal{" +
				"name='" + name + '\'' +
				", years=" + years +
				'}';
	}
}
