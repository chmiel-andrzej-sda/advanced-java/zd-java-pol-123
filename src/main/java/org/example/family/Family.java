package org.example.family;

import java.util.List;
import java.util.Objects;

public class Family {
	private final String name;
	private final List<Human> humans;

	public Family(final String name, final List<Human> humans) {
		this.name = name;
		this.humans = humans;
	}

	public String getName() {
		return name;
	}

	public List<Human> getHumans() {
		return humans;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Family family)) {
			return false;
		}
		return Objects.equals(name, family.name) && Objects.equals(humans, family.humans);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, humans);
	}

	@Override
	public String toString() {
		return "Family{" +
				"name='" + name + '\'' +
				", humans=" + humans +
				'}';
	}
}
