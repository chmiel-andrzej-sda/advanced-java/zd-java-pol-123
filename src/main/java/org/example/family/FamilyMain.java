package org.example.family;

import java.util.Collection;
import java.util.List;

public class FamilyMain {
	public static void main(final String[] args) {
		final List<Family> families = createFamilies();
		families.stream()
				.map(Family::getHumans)
				.flatMap(Collection::stream)
				.filter(human -> human.getName().endsWith("2") || human.getName().endsWith("3"))
				.map(Human::getAnimals)
				.flatMap(Collection::stream)
				.filter(animal -> animal.getYears() < 8)
				.findFirst()
				.map(Animal::toString)
				.ifPresent(System.out::println);
	}

	private static List<Family> createFamilies() {
		return List.of(
				new Family("Test1", List.of(
						new Human("Name 1", List.of(
								new Animal("Cat", 1),
								new Animal("Dog", 2)
						)),
						new Human("Name 2", List.of(
								new Animal("Fish", 7),
								new Animal("Tiger", 10)
						))
				)),
				new Family("Test2", List.of(
						new Human("Name 3", List.of(
								new Animal("Pig", 3),
								new Animal("Bird", 5)
						)),
						new Human("Name 8", List.of(
								new Animal("Hamster", 1),
								new Animal("Rat", 3)
						))
				))
		);
	}
}
