package org.example.game.item;

public class Chestplate extends ArmorItem {
	public Chestplate(final String name, final String description, final Material material, final int value, final int armor) {
		super(name, description, material, value, armor);
	}

	@Override
	public String toString() {
		return "Chestplate{} " + super.toString();
	}
}
