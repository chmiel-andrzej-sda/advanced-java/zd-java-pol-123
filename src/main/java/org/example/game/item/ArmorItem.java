package org.example.game.item;

import java.util.Objects;

public class ArmorItem extends Item {
	private int armor;
	private Material material;
//	private Calculations1 calculations = new Calculations1();

	private Calculations2 calculations;
//	private class Calculations1 {
//		public int calculate() {
//			return (int) (baseValue * armor / 100.0);
//		}
//	}

	private static class Calculations2 {
		int baseValue;

		Calculations2(final int baseValue) {
			this.baseValue = baseValue;
		}

		int calculate(final int armor) {
			return (int) (baseValue * armor / 100.0);
		}
	}

	public ArmorItem(final String name, final String description, final Material material, final int value, final int armor) {
		super(name, description, value);
		calculations = new Calculations2(value);
		this.material = material;
		if (armor > 100) {
			this.armor = 100;
		} else {
			this.armor = Math.max(armor, 0);
		}
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(final int armor) {
		this.armor = armor;
		super.value = calculations.calculate(armor);
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(final Material material) {
		this.material = material;
	}

	@Override
	public void setValue(final int value) {
		super.setValue(value);
		calculations.baseValue = value;
	}

	@Override
	public String toString() {
		return "ArmorItem{" +
				"armor=" + armor +
				", material=" + material +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ArmorItem armorItem)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return armor == armorItem.armor
				&& material == armorItem.material;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), armor, material);
	}
}
