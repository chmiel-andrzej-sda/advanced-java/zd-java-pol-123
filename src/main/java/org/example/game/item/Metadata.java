package org.example.game.item;

import java.util.Objects;

public class Metadata<T> {
	private T value;

	public Metadata(final T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(final T value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Metadata{" +
				"value=" + value +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Metadata<?> metadata)) {
			return false;
		}
		return Objects.equals(value, metadata.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}
}
