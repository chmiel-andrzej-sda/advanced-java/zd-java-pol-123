package org.example.game.item;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Item {
	protected String name;
	private String description;
	protected int value;
	private final Map<Class<?>, Metadata<?>> metadata = new HashMap<>();

	public Item(final String name, final String description, final int value) {
		this.name = name;
		this.description = description;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public final void setDescription(final String description) {
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public void setValue(final int value) {
		this.value = value;
	}

	public Map<Class<?>, Metadata<?>> getMetadata() {
		return new HashMap<>(metadata);
	}

	public <T> void addMetadata(final Class<T> clazz, final Metadata<T> metadata) {
		this.metadata.put(clazz, metadata);
	}

	public <T> Metadata<T> getMetadata(final Class<T> clazz) {
		return (Metadata<T>) metadata.get(clazz);
	}

	@Override
	public String toString() {
		return "Item{" +
				"name='" + name + '\'' +
				", description='" + description + '\'' +
				", value=" + value +
				", metadata=" + metadata +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Item item)) {
			return false;
		}
		return Objects.equals(name, item.name)
				&& Objects.equals(description, item.description)
				&& Objects.equals(metadata, item.metadata);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, description, value, metadata);
	}
}
