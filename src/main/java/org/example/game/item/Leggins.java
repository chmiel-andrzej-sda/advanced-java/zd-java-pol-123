package org.example.game.item;

public class Leggins extends ArmorItem {
	public Leggins(final String name, final String description, final Material material, final int value, final int armor) {
		super(name, description, material, value, armor);
	}

	@Override
	public String toString() {
		return "Leggins{} " + super.toString();
	}
}
