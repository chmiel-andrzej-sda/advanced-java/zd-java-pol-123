package org.example.game.item;

import java.util.Objects;

public class Sword extends Item {
	private Material material;

	public Sword(final String name, final String description, final int value, final Material material) {
		super(name, description, value);
		this.material = material;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(final Material material) {
		this.material = material;
	}

	@Override
	public String toString() {
		return "Sword{" +
				"material=" + material +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Sword sword)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return material == sword.material;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), material);
	}
}
