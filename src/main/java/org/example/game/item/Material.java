package org.example.game.item;

public enum Material {
	IRON("żelazo", 0.3f),
	SILVER("srebro", 0.5f),
	BRONZE("brąz", 0.75f);

	private final String name;
	private final float defense;

	Material(final String name, final float defense) {
		this.name = name;
		this.defense = defense;
	}

	public String getName() {
		return name;
	}

	public float getDefense() {
		return defense;
	}
}
