package org.example.game.item;

public class Boots extends ArmorItem {
	public Boots(final String name, final String description, final Material material, final int value, final int armor) {
		super(name, description, material, value, armor);
	}

	@Override
	public String toString() {
		return "Boots{} " + super.toString();
	}
}
