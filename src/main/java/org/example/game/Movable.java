package org.example.game;

public interface Movable {
	void setLocation(final Location location);

	Location getLocation();
}
