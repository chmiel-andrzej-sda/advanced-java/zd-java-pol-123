package org.example.game.entity;

import org.example.game.Inventory;
import org.example.game.item.Boots;
import org.example.game.item.Chestplate;
import org.example.game.item.Helmet;
import org.example.game.item.Item;
import org.example.game.item.Leggins;

import java.util.Objects;

public abstract class LivingEntity extends Entity implements Speakable {
	protected int health = 100;
	protected int mana = 100;
	private int level = 1;
	private Money<Integer> gold = new Money<>(0);
	private Inventory inventory = new Inventory();
	private Item itemInHand;
	private Helmet helmet;
	private Chestplate chestplate;
	private Leggins leggins;
	private Boots boots;

	protected LivingEntity(final String name) {
		super(name);
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(final int health) {
		if (health > 100 || health < 0) {
			return;
		}
		this.health = health;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(final int mana) {
		if (mana > 100 || mana < 0) {
			return;
		}
		this.mana = mana;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(final Inventory inventory) {
		this.inventory = inventory;
	}

	public Item getItemInHand() {
		return itemInHand;
	}

	public void setItemInHand(final Item itemInHand) {
		this.itemInHand = itemInHand;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(final int level) {
		this.level = level;
	}

	public Money<Integer> getGold() {
		return gold;
	}

	public void setGold(final Money<Integer> gold) {
		this.gold = gold;
	}

	public Helmet getHelmet() {
		return helmet;
	}

	public void setHelmet(final Helmet helmet) {
		this.helmet = helmet;
	}

	public Chestplate getChestplate() {
		return chestplate;
	}

	public void setChestplate(final Chestplate chestplate) {
		this.chestplate = chestplate;
	}

	public Leggins getLeggins() {
		return leggins;
	}

	public void setLeggins(final Leggins leggins) {
		this.leggins = leggins;
	}

	public Boots getBoots() {
		return boots;
	}

	public void setBoots(final Boots boots) {
		this.boots = boots;
	}

	@Override
	public String toString() {
		return "LivingEntity{" +
				"health=" + health +
				", mana=" + mana +
				", level=" + level +
				", gold=" + gold +
				", inventory=" + inventory +
				", itemInHand=" + itemInHand +
				", helmet=" + helmet +
				", chestplate=" + chestplate +
				", leggins=" + leggins +
				", boots=" + boots +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof LivingEntity that)) {
			return false;
		}
		return health == that.health
				&& mana == that.mana
				&& level == that.level
				&& gold == that.gold
				&& Objects.equals(inventory, that.inventory)
				&& Objects.equals(itemInHand, that.itemInHand)
				&& Objects.equals(helmet, that.helmet)
				&& Objects.equals(chestplate, that.chestplate)
				&& Objects.equals(leggins, that.leggins)
				&& Objects.equals(boots, that.boots);
	}

	@Override
	public int hashCode() {
		return Objects.hash(health, mana, level, gold, inventory, itemInHand, helmet, chestplate, leggins, boots);
	}
}
