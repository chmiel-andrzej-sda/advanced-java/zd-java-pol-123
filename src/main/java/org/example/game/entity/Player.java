package org.example.game.entity;

public class Player extends LivingEntity implements Jumpable {
	public Player(final String name) {
		super(name);
	}

	@Override
	public void speak(final String text) {
		System.out.println(getName() + ": " + text);
	}

	@Override
	public String toString() {
		return "Player{} " + super.toString();
	}

	@Override
	public void jump() {
		System.out.println(getName() + " is jumping");
	}
}
