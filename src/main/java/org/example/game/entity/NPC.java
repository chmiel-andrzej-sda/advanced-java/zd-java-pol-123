package org.example.game.entity;

public class NPC extends LivingEntity {
	public NPC(final String name) {
		super(name);
	}

	@Override
	public void speak(final String text) {
		System.out.println(text);
	}

	@Override
	public String toString() {
		return "NPC{} " + super.toString();
	}
}
