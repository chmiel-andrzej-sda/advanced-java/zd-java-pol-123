package org.example.game.entity;

import java.util.Random;

public class Animal extends LivingEntity {
	private static final Random RANDOM = new Random();

	public Animal(final String name) {
		super(name);
	}

	@Override
	public void speak(final String text) {
		final int count = RANDOM.nextInt(3) + 2;
		final StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < count; i++) {
			stringBuilder.append(text).append(" ");
		}
		System.out.println(stringBuilder);
	}
}
