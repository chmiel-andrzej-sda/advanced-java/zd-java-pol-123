package org.example.game.entity;

public interface Speakable {
	void speak(final String text);
}
