package org.example.game.entity;

import org.example.game.Location;
import org.example.game.Movable;

import java.util.Objects;

public class Entity implements Movable {
	private final String name;
	private Location location = new Location(0, 0);

	public Entity(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public void setLocation(final Location location) {
		if (location == null) {
			return;
		}
		this.location = location;
	}

	@Override
	public String toString() {
		return "Entity{" +
				"name='" + name + '\'' +
				", location=" + location +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Entity entity)) {
			return false;
		}
		return Objects.equals(name, entity.name) && Objects.equals(location, entity.location);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, location);
	}
}
