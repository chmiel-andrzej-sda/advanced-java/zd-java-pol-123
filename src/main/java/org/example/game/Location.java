package org.example.game;

public record Location(int x, int y) { }
