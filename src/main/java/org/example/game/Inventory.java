package org.example.game;

import org.example.game.item.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Inventory {
	private final List<Item> items = new ArrayList<>();

	public void add(final Item item) {
		items.add(item);
	}

	public boolean remove(final Item item) {
		return items.remove(item);
	}

	public List<Item> getItems() {
		return new ArrayList<>(items);
	}

	@Override
	public String toString() {
		return "Inventory{" +
				"items=" + items +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Inventory inventory)) {
			return false;
		}
		return Objects.equals(items, inventory.items);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(items);
	}
}
